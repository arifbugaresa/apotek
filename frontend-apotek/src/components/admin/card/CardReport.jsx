import CurrencyFormat from "react-currency-format";
import { Link } from "react-router-dom";

const CardReport = (props) => {
    return (
        <div class={props.card}>
            <div class="inner">
                {props.type === "text" ?
                    <h3>{props.amount}</h3> :
                    <CurrencyFormat
                        value={props.amount}
                        displayType={"text"}
                        thousandSeparator={true}
                        prefix={"Rp. "}
                        renderText={(value) => (
                            <h3>{value}</h3>
                        )}
                    />
                }
                <p>{props.title}</p>
            </div>
            <Link
                to={props.link}
                className="card-box-footer">
                View More <i class="fa fa-arrow-circle-right"></i>
            </Link>
        </div>
    );
}

export default CardReport;