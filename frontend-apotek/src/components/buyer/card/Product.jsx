import React from 'react'
import {
    Card, CardImg,
    CardBody,
    CardSubtitle,
    CardText,
    Button
} from 'reactstrap';
import CurrencyFormat from 'react-currency-format';
import { useDispatch } from 'react-redux';
import { keranjangAction } from '../../../redux/reducer/KeranjangReducer';
import swal from 'sweetalert';

const Product = (props) => {

    const dispatch = useDispatch();

    const keranjangHandler = (product) => {
        dispatch(keranjangAction.tambah(product));
        swal("Sukses", "Obat berhasil ditambahkan ke keranjang", "success");
    }

    return (
        <div>
            <Card
                className="mb-4 border-1 p-3 text-center"
            >
                <CardImg
                    className="image-product"
                    src={`data:image/*;base64, ` + props.product.dataImage}
                />
                <CardBody className="card-body-home">
                    <CardSubtitle
                        tag="h6"
                        className="mb-2 movie-name">
                        <b>{props.product.namaObat}</b>
                    </CardSubtitle>
                    <CardText>
                        <p>
                            {
                                props.product.stokObat === 0 ?
                                    <span>Habis Terjual</span> :
                                    <span>Tersisa {props.product.stokObat}</span>
                            }
                            <br />
                            <CurrencyFormat
                                value={props.product.hargaObat}
                                displayType={"text"}
                                thousandSeparator={true}
                                prefix={"Rp. "}
                                renderText={(value) => (
                                    <span>{value}</span>
                                )}
                            />
                        </p>
                    </CardText>
                    <div className="text-center">
                        {
                            props.product.stokObat === 0 ?
                                <Button className="btn-keranjang" onClick={() => keranjangHandler(props.product)} disabled>Tambah</Button> :
                                <Button className="btn-keranjang" onClick={() => keranjangHandler(props.product)} >Tambah</Button>
                        }
                    </div>
                </CardBody>
            </Card>
        </div>
    );
}

export default Product;