import React from 'react';
import swal from 'sweetalert';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { authAction } from '../../../redux/reducer/AuthReducer';
import { Route, Switch, useRouteMatch } from 'react-router';
import Home from './Home';
import Keranjang from './Keranjang';
import Pesanan from './Pesanan';

const ContentBuyer = () => {

    let { path } = useRouteMatch();

    const login = useSelector((state) => state.auth.user.login);

    const dispatch = useDispatch();

    if (!login) {
        swal("Error!", "Silahkan login kembali!", "error");
        dispatch(authAction.logout());
    }

    return (
        <Switch>
            <Route exact path={`${path}`} component={Home} />
            <Route exact path={`${path}/keranjang`} component={Keranjang} />
            <Route exact path={`${path}/pesanan`} component={Pesanan} />

            {/* <Route exact path={`${path}/obat`} component={Obat} />
            <Route exact path={`${path}/user`} component={Pelanggan} /> */}


            {/* <Route exact path={`${path}/room`} component={ContentRoomAdmin} />
            <Route exact path={`${path}/movie`} component={ContentMovieAdmin} />
            <Route exact path={`${path}/schedule`} component={ContentScheduleAdmin} />
            <Route exact path={`${path}/user`} component={ContentUserAdmin} />
            <Route exact path={`${path}/account`} component={ContentAccountAdmin} />
            <Route exact path={`${path}/order`} component={ContentOrderAdmin} />
            <Route exact path={`${path}/order-request`} component={ContentOrderRequest} />
            <Route exact path={`${path}/seat-regular/:id`} component={ContentSeatRegular} />
            <Route exact path={`${path}/seat-vip/:id`} component={ContentSeatVip} /> */}
        </Switch>
    );
}

export default ContentBuyer;