import axios from "axios";

const BASE_URL = '/api/';

export async function getData(target) {
    const url = BASE_URL + target;
    return await axios.get(url);
}

export function postData(target, data) {
    const url = BASE_URL + target;
    return axios.post(url, data);
}