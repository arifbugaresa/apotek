import { createSlice } from "@reduxjs/toolkit";

const initialAuthState = {
    user: {
        login : false
    }
}

const authSlice = createSlice({
    name: "auth",
    initialState: initialAuthState,
    reducers: {
        login(state, action) {
            state.user = action.payload;
        },
        logout(state) {
            state.user = '';
        }
    }
})

export const authAction = authSlice.actions;

export default authSlice.reducer;