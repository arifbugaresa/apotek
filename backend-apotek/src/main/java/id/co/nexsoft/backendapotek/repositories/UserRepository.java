package id.co.nexsoft.backendapotek.repositories;

import id.co.nexsoft.backendapotek.dto.response.LoginResponse;
import id.co.nexsoft.backendapotek.entities.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    @Modifying
    @Query(value = "INSERT INTO user(id, alamat, email, nama_lengkap, no_telp, password, role, tgl_registrasi, username) " +
            "VALUES(:#{#u.id}, :#{#u.alamat}, :#{#u.email}, :#{#u.namaLengkap}, :#{#u.no_telp}, :#{#u.password}, :#{#u.role}, :#{#u.tgl_registrasi} , :#{#u.username})"
            , nativeQuery = true)
    void saveUser(@Param("u") User user);

    @Query(value ="SELECT * FROM user u WHERE u.username = :u", nativeQuery = true)
    User checkByUsername(@Param("u") String username);

    @Query(value ="SELECT * FROM user u WHERE u.email = :e", nativeQuery = true)
    User checkByEmail(@Param("e") String email);

    @Query(value ="SELECT * FROM user u WHERE u.no_telp = :n", nativeQuery = true)
    User checkByPhone(@Param("n") String no_telp);

    @Query( value = "SELECT EXISTS(SELECT id FROM user u WHERE u.username = :username )" , nativeQuery = true)
    int checkUsername(@Param("username") String username);

    @Query(value = "SELECT * FROM user u WHERE u.username = :username", nativeQuery = true)
    User findUserByUsername(@Param("username") String username);

    @Modifying
    @Query(value = "UPDATE user u SET u.is_login = :isLogin WHERE u.username = :username", nativeQuery = true)
    void userLogin(@Param("username") String username, @Param("isLogin") int isLogin);

    @Modifying
    @Query(value = "UPDATE user u SET u.is_login = :isLogin WHERE u.id = :id", nativeQuery = true)
    void userLogout(@Param("id") int id, @Param("isLogin") int isLogin);

    @Query( value = "SELECT new id.co.nexsoft.backendapotek.dto.response.LoginResponse" +
            "(u.id, u.namaLengkap, u.username, u.alamat, u.no_telp, u.email, u.role, u.isLogin) " +
            "FROM User u WHERE u.username = :username")
    LoginResponse loginResponse(@Param("username") String username);

    @Query(value = "SELECT * FROM user u WHERE u.id = :id", nativeQuery = true)
    User findUser(@Param("id") int id);

    @Query(value = "SELECT * FROM user u ORDER BY u.role ASC, u.nama_lengkap ASC", nativeQuery = true)
    List<User> findAllUser();
}
