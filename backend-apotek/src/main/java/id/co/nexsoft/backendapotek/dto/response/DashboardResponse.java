package id.co.nexsoft.backendapotek.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter

public class DashboardResponse {

    private int jumlahOrder;

    private long pendapatan;

}
