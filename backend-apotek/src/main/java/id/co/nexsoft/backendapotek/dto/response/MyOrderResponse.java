package id.co.nexsoft.backendapotek.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class MyOrderResponse {

    private int id;

    private LocalDate tanggalOrder;

    private String status;

    private long jumlahOrder;

    public MyOrderResponse(int id, LocalDate tanggalOrder, String status) {
        this.id = id;
        this.tanggalOrder = tanggalOrder;
        this.status = status;
    }
}
