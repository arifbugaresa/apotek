package id.co.nexsoft.backendapotek.services;

import id.co.nexsoft.backendapotek.dto.request.LoginRequest;
import id.co.nexsoft.backendapotek.dto.response.HTTPResponse;
import id.co.nexsoft.backendapotek.dto.response.HTTPResponses;
import id.co.nexsoft.backendapotek.dto.response.LoginResponse;
import id.co.nexsoft.backendapotek.entities.User;
import id.co.nexsoft.backendapotek.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Transactional
    public ResponseEntity<?> registerBuyer(User user) {

        // cek username
        User usernameExist = userRepository.checkByUsername(user.getUsername());
        if (usernameExist != null)
            return ResponseEntity.badRequest()
                .body(new HTTPResponse(false, "Username sudah digunakan user lain", null));

        // cek email
        User emailExist = userRepository.checkByEmail(user.getEmail());
        if (emailExist != null)
            return ResponseEntity.badRequest()
                    .body(new HTTPResponse(false, "Email sudah digunakan user lain", null));

        // check by phone
        User phoneExist = userRepository.checkByPhone(user.getNo_telp());
        if (phoneExist != null)
            return ResponseEntity.badRequest()
                    .body(new HTTPResponse(false, "Nomor sudah digunakan user lain", null));

        user.setTgl_registrasi(LocalDate.now());
        user.setRole("buyer");
        userRepository.saveUser(user);
        return ResponseEntity.ok()
                .body(new HTTPResponse(true, "Registrasi Berhasil!", null));
    }

    @Transactional
    public ResponseEntity<?> registerAdmin(User user) {

        // cek username
        User usernameExist = userRepository.checkByUsername(user.getUsername());
        if (usernameExist != null)
            return ResponseEntity.badRequest()
                    .body(new HTTPResponse(false, "Username sudah digunakan user lain", null));

        // cek email
        User emailExist = userRepository.checkByEmail(user.getEmail());
        if (emailExist != null)
            return ResponseEntity.badRequest()
                    .body(new HTTPResponse(false, "Email sudah digunakan user lain", null));

        // check by phone
        User phoneExist = userRepository.checkByPhone(user.getNo_telp());
        if (phoneExist != null)
            return ResponseEntity.badRequest()
                    .body(new HTTPResponse(false, "Nomor sudah digunakan user lain", null));

        user.setRole("admin");
        userRepository.saveUser(user);
        return ResponseEntity.ok()
                .body(new HTTPResponse(true, "Registrasi Berhasil!", null));
    }

    @Transactional
    public ResponseEntity<?> loginUser(LoginRequest loginRequest) {

        // cek username di db
        if(userRepository.checkUsername(loginRequest.getUsername()) != 1) {
            return ResponseEntity.badRequest()
                    .body(new HTTPResponse(false, "Username tidak ada", null));
        }

        // cek password apakah sesuai
        User userRepo = userRepository.findUserByUsername(loginRequest.getUsername());
        if(!loginRequest.getPassword().equals(userRepo.getPassword())) {
            return ResponseEntity.badRequest()
                    .body(new HTTPResponse(false, "Password salah", null));
        }

        userRepository.userLogin(userRepo.getUsername(), 1);

        LoginResponse loginResponse = userRepository.loginResponse(loginRequest.getUsername());

        return ResponseEntity.ok()
                .body(new HTTPResponse(true, "Berhasil login", loginResponse));
    }

    @Transactional
    public ResponseEntity<?> logoutUser(int id) {

        userRepository.userLogout(id, 0);

        return ResponseEntity.ok()
                .body(new HTTPResponse(true, "Berhasil Logout", null));
    }

    public ResponseEntity<?> findAllUser() {

        List<User> listUser = userRepository.findAllUser();

        return ResponseEntity.ok()
                .body(new HTTPResponses(true, null, listUser));
    }
}
