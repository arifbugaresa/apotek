package id.co.nexsoft.backendapotek.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter

public class HTTPResponses {

    private boolean success;

    private String message;

    private List<?> data;

}
