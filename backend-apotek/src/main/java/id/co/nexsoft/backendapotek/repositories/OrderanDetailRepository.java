package id.co.nexsoft.backendapotek.repositories;

import id.co.nexsoft.backendapotek.dto.request.OrderanDetailRequest;
import id.co.nexsoft.backendapotek.entities.OrderanDetail;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderanDetailRepository extends CrudRepository<OrderanDetail, Integer> {

    @Modifying
    @Query( value = "INSERT INTO orderan_detail(id, orderan_id, obat_id) " +
            "VALUES(:#{#order.id}, :#{#order.orderanId}, :#{#order.obatId})", nativeQuery = true)
    void checkoutOrderanDetail(@Param("order") OrderanDetailRequest order);

    @Query( value = "SELECT count(id) FROM orderan_detail od WHERE od.orderan_id = :id" , nativeQuery = true )
    int getJumlahOrder(int id);

}
