package id.co.nexsoft.backendapotek.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
public class Obat {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private int id;

    @NotNull
    @NotBlank
    @NotEmpty
    private String namaObat;

    @NotNull
    @NotBlank
    @NotEmpty
    private String jenisObat;

    @NotNull
    private int stokObat;

    @NotNull
    private int hargaObat;

    @NotNull
    @NotBlank
    @NotEmpty
    private String dosis;

    @Lob
    private byte[] dataImage;

    private LocalDate tglDihapus;
}
