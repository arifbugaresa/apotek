package id.co.nexsoft.backendapotek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendApotekApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendApotekApplication.class, args);
    }

}
