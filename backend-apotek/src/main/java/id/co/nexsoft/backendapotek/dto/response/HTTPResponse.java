package id.co.nexsoft.backendapotek.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter

public class HTTPResponse {

    private boolean success;

    private String message;

    private Object data;

}
